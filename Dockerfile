FROM artefact.skao.int/ska-base:0.1.1

# Get cheetah version from repo
WORKDIR /
COPY VERSION.txt VERSION.txt

# Install cheetah and dependencies (from CAR/apt)
RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
                   fftw3 libfftw3-dev \
                   make cmake \
                   gcc \
                   libboost-dev libboost-program-options-dev \
                   libboost-system-dev libboost-filesystem-dev \
                   wget curl \
                   nasm vim \
                   openssh-server openssh-client net-tools && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN CHEETAH_VERSION=$(cat VERSION.txt) && \
    wget --progress=dot:giga https://artefact.skao.int/repository/apt-bionic-internal/pool/c/cheetah/cheetah_"${CHEETAH_VERSION}"-nasm_amd64.deb && \ 
    dpkg -i cheetah_"${CHEETAH_VERSION}"-nasm_amd64.deb

# Set up and configure cheetah user
RUN useradd -rm -d /home/cheetah -s /bin/bash -g root -G sudo -u 1001 cheetah
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN echo 'cheetah:cheetah' | chpasswd
USER cheetah

# Get test vector
RUN curl --resolve testvectors.jb.man.ac.uk:80:130.88.24.62 \
    -o /tmp/SPS-MID_747e95f_0.125_0.00125_2000.0_0.0_Gaussian_50.0_0000_123123123.fil \
    http://testvectors.jb.man.ac.uk/SPS-MID/SPS-MID_747e95f_0.125_0.00125_2000.0_0.0_Gaussian_50.0_0000_123123123.fil

# Provide some config examples
WORKDIR /home/cheetah
RUN curl -o /home/cheetah/sps_example_config.xml \
    https://gitlab.com/ska-telescope/pss/ska-pss-cheetah/-/raw/dev/config_examples/xml/cheetah_pipeline_single_beam_sps.xml && \
    sed -i 's/\/raid\/tvcache\//\/tmp\//' /home/cheetah/sps_example_config.xml 

# Set up sshd
USER root
COPY docker-entrypoint.sh /docker-entrypoint.sh 
RUN chmod +x /docker-entrypoint.sh && \
    mkdir -p /run/sshd && \
    chmod 0755 /run/sshd
RUN ssh-keygen -A
EXPOSE 22

CMD ["tail", "-f", "/dev/null"]

ENTRYPOINT ["sh", "/docker-entrypoint.sh"]
