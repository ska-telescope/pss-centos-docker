
# Include core make support
include .make/base.mk

# Include OCI images support
include .make/oci.mk

OCI_IMAGES = ska-pss-image
PROJECT_NAME = ska-pss-image
PROJECT_PATH = ska-telescope/pss/ska-pss-image
ARTEFACT_TYPE = oci

# Define default make target
.DEFAULT_GOAL := help


