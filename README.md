PSS prototype docker images for integration and testing
==========================================================

This repository contains tools which will construct a docker image of the Pulsar Searching Subsystem (PSS) pipeline for the purposes of testing the [PSS control layer](https://gitlab.com/ska-telescope/ska-pss-lmc). It is not intended that a containerised PSS will run in production. We provide this image only to facilitate testing. The version of this image will track the version of cheetah that is installed into the image. 

## Accessing the image

The image can be added to a local registry in two ways. One can make the image locally by running

```bash
make oci-build
```

or it can be pulled directly from the gitlab registry as follows

```bash
docker pull registry.gitlab.com/ska-telescope/pss/ska-pss-image/ska-pss-image:<version>
```

where the current version is shown in VERSION.txt

## What does the image provide?

The image provides a container running Ubuntu 22.04. The main PSS search application, [cheetah](https://gitlab.com/ska-telescope/pss/ska-pss-cheetah), is installed into the container from the [Central Artefact Repository](http://artefact.skao.int). The specific "spin" of cheetah (i.e., the particular build of the application that utilises certain hardware resources) that is provided is capable of executing a CPU-based single-pulse search (SPS) pipeline.  Specifically, we provide

* A cheetah executable installed into the system path that is available to all users. This binary is located at /usr/bin/cheetah\_pipeline
* A test vector - this is a test dataset containing a number of single pulses expected to be detectable by SPS. The specific details of the pulses contained are not relevant here as the vector is provided in order to support the control of  a running pipeline, rather than to demostrate the efficacy of the pipeline's detection capabilities. The test vector, located in /tmp, has the following properties:
   * A duration of 60s, sampled at 64 microsecond intervals
   * A bandwidth of 320 MHz, centred on 1520 MHz, channelised into 4096 sub-bands (SKA-MID)
   * 3.8 GB file size
* A cheetah configuration file. This is an XML file which is passed to cheetah on execution, that enables and configures the cheetah modules that will form a search pipeline. We provide this for illustrative purposes only, as, when controlled by a LMC, a configuration for cheetah will be passed to cheetah from CSP.LMC. This configuration file is located in /home/cheetah

On launching a container, the user is logged in, as a cheetah user, into /home/cheetah.

## Running a single pulse search pipeline manually

Prior to enabling control of the PSS with a LMC layer, here we demostrated how we run the pipeline as a user of the container image directly.

If using the SKA minikube environment, launch the [minikube environment](http://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube) as follows (from the ska-cicd-deploy-minikube repository)

```bash
make all DRIVER=docker CPUS=16 MEM=16384
eval $(minikube docker-env)
```

Returning to ska-pss-image, make (or pull) the image according to the instructions above. Launch a container as follows.

```bash
docker run -it --rm  --name cheetah-container ska-pss-image:<version>
```

Verify that cheetah\_pipeline is installed and is available in the PATH and that the config file exists in the home area, and that testvector (.fil) file exists in /tmp

```bash
cheetah_pipeline -h
cat sps_example_config.xml
ls /tmp
```

Now we can launch a single pulse search pipeline as follows

```bash
cheetah_pipeline --config sps_example_config.xml -p SinglePulse -s sigproc --log-level debug
```

This will print output to the terminal showing the progress of the pipeline. 

Something to note that is relevant to the LMC; This pipeline does not run as a daemon. Once the pipeline has finished its search, it will exit and return control to the terminal. This is because the pipeline is reading from a test vector (given by the command argument -s sigproc) rather than a UDP stream (in which case the command argument would be  -s udp\_low). However the pipeline should hopefully last long enough to test the start/stop functionality of the LMC. Tests of the pipeline durations using this test vector show that it takes approximately 3x real time (around 3 minutes) to complete. 

## Running SPS via the LMC

We haven't tested this yet but here we provide some guidance on the contents of the LMC helm chart that launches cheetah in simulation mode. We refer to the helm chart located [here](https://gitlab.com/ska-telescope/ska-pss-lmc/-/blob/master/charts/ska-pss-lmc/data/psspipelinectrlsim.yaml?ref_type=heads). To launch the pipeline using this helm chart the following parameters should be included

```yaml
    - name: "NodeIP"
      values:
      - "cheetah-deployment-0.cheetah-service"
    - name: "PipelineName"
      values:
      - "pipeline-00-00-01"
    - name: "CheetahOutputFile"
      values:
      - "/tmp/cheetah.out"
    - name: "CheetahConfigFile"
      values:
      - "/tmp/cheetah_config.xmi"
    - name: "CheetahExecutable"
      values:
      - "/usr/bin/cheetah_pipeline"
    - name: "CheetahPipelineType"
      values:
      - "SinglePulse"
    - name: "CheetahPipelineSource"
      values:
      - "sigproc"
    - name: "CheetahLogLevel"
      values:
      - "debug"          
    - name: "CheetahUserPasswd"
      values:
      - "cheetah"
      - "cheetah"
```

Refer to [this documentation](https://gitlab.com/ska-telescope/ska-pss-lmc/-/blob/master/docs/src/operation/example.rst?ref_type=heads) for full details on how to control a cheetah pipeline via the LMC.
